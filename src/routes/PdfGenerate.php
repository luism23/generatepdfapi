<?php
require_once(preg_replace('/wp-content.*$/','',__DIR__).'wp-load.php');


$data = $_GET["infoForm"];
$data = base64_decode($data);

$data = json_decode($data,true);

$newData = [];
for ($i=0; $i < count($data); $i++) { 
    $newData[$data[$i]["name"]] = $data[$i];
}
$data = $newData;

GPDFA_put_optionPage("GPDFA-pdf-option",$data);

if($_GET["var_dump"]=="yes"){

	echo "<pre>";
	var_dump($data);
	echo "</pre>";

}

if($data["tipePdf"]["value"] == 'tipe1'){
	require GPDFA_PATH."src/templates/pdf1.php";
}else if($data["tipePdf"]["value"] == 'tipe2'){
	require GPDFA_PATH."src/templates/pdf2.php";
}else if($data["tipePdf"]["value"] == 'tipe3'){
	require GPDFA_PATH."src/templates/pdf3.php";
}else {
	echo "pdf invalido";
}

