<?php

function GPDFA_get_optionPage($id = "GPDFA_optionPage")
{
    $GPDFA_optionPage = get_option($id);
    if($GPDFA_optionPage === false || $GPDFA_optionPage == null || $GPDFA_optionPage == ""){
        $GPDFA_optionPage = "[]";
    }
    $GPDFA_optionPage = json_decode($GPDFA_optionPage,true);
    return $GPDFA_optionPage;
}

function GPDFA_put_optionPage($id,$newItem)
{
    $GPDFA_optionPage = GPDFA_get_optionPage($id);
    $GPDFA_optionPage[] = array(
        "date" => date("c"),
        "data" => $newItem,
    );
    update_option($id,json_encode($GPDFA_optionPage));
}
