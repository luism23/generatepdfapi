<div id="openModal" class="modalDialog">
	<div class="modal1">
		<h2>Warning</h2>
		<p>You are going to download the pdf, keep in mind that if you cancel the download, the form will be lost.</p>
        <button class="buttond" onclick="downloadPDF()">Download</button>
	</div>
</div>



<style>
    .modalDialog {
        position: fixed;
        font-family: Arial, Helvetica, sans-serif;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        background: rgba(0,0,0,0.8);
        z-index: 99999;
        opacity:0;
        -webkit-transition: opacity 400ms ease-in;
        -moz-transition: opacity 400ms ease-in;
        transition: opacity 400ms ease-in;
        pointer-events: none;
    }
    .modalDialog {
        opacity:1;
        pointer-events: auto;
    }
    .modalDialog > div {
        width: 400px;
        position: relative;
        margin: 10% auto;
        padding: 5px 20px 13px 20px;
        border-radius: 10px;
        background: #fff;
        background: -moz-linear-gradient(#fff, #999);
        background: -webkit-linear-gradient(#fff, #999);
        background: -o-linear-gradient(#fff, #999);
        -webkit-transition: opacity 400ms ease-in;
        -moz-transition: opacity 400ms ease-in;
        transition: opacity 400ms ease-in;
    }
    .buttond{
        border-radius:20px;
        border:0 ;
        background: #AF983F;
        padding: 10px;
        margin:10px 0;
        cursor: pointer;
        font-Family: arial,verdana;
        font-weight: 700;
        font-size:18px;

    }
    .modal1{
        align-items:center;
        font-Family: arial,verdana;
        font-weight: 400;
        font-size:18px;

    }
</style>

<script>
    window.onafterprint = function(e){
        window.location = "<?=get_home_url()?>";
    }
    const downloadPDF = () => {
        const openModal = document.getElementById("openModal")
        openModal.style.display = "none"
        window.print()
    }
	
</script>