<?php
/*
Plugin Name: GeneratePdfApi
Plugin URI: https://gitlab.com/luism23/generatepdfapi
Description: GeneratePdfApi
Author: LuisMiguelNarvaez
Version: 1.0.6
Author URI: https://gitlab.com/luism23
License: GPL
 */

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/luism23/generatepdfapi',
	__FILE__,
	'generatepdfapi'
);
$myUpdateChecker->setAuthentication('glpat-puSVzsHaWSc66qa_rVr1');
$myUpdateChecker->setBranch('master');


define("GPDFA_LOG",false);
define("GPDFA_PATH",plugin_dir_path(__FILE__));
define("GPDFA_URL",plugin_dir_url(__FILE__));


require GPDFA_PATH."src/_index.php";